from django.contrib import admin
from django.utils.safestring import mark_safe
from .models import DataFeed
# from models import DataFeed
# Register your models here.


class DataFeedAdmin(admin.ModelAdmin):
    fields = ['date', 'ticker', 'price']
    list_display = ['date', 'ticker', 'price']


admin.site.register(DataFeed, DataFeedAdmin)
