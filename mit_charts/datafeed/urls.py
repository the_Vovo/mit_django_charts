from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='datafeed-home'),
    path('api/data/<ticker>/', views.get_data, name="datafeed-specific-data"),
    path('api/data/', views.get_init_data, name="datafeed-data"),
    path('about/', views.about, name='datafeed-about'),
]
