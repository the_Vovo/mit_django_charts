import fasttrack
import psycopg2
from datetime import date, datetime
import csv
import pandas
from pandas import DataFrame


def create_conn():
    # try:
    return psycopg2.connect(dbname='mit_test', user='postgres', password='postgres', host='localhost', port='5433')
    # except psycopg2.Error as e:
    #     print(e.pgerror)


def create_cursor(self):
    try:
        return self.create_conn().cursor()
    except psycopg2.Error as e:
        print(e.pgerror)


def save_market_data(date, ticker, price):
    conn = create_conn()
    cursor = conn.cursor()
    print(str(date) + str(ticker) + str(price))

    try:
        cursor.execute(
            "INSERT INTO datafeed_datafeed(date, ticker, price)"
            "VALUES (%(date)s, %(ticker)s, %(price)s)",
            {"date": date, "ticker": ticker, "price": price})
        conn.commit()
        cursor.close()
    except psycopg2.Error as e:
        print(e.pgerror)

#
# def daily_ticker(ticker, price, all_dates=True, date=''):
#     global app_id
#     date_list = fasttrack.get_bulk_market_dates(app_id=app_id)
#     price_array = fasttrack.get_bulk_market_data(app_id, ticker)
#     temp_data = {'date': date_list, 'ticker': ticker, "price": price_array}
#     all_tickers = fasttrack.get_all_tickers(app_id=fasttrack.get_app_id())
#     for ticker in all_tickers:
#         df = fasttrack.compile_market_data(ticker=ticker, all_dates=all_dates, date=date)
#         if date not in date_list and int(date[0:4]) == int(date_list[-1].year):
#             date_list.append(date)
#             date_ind = date_list.index(date)
#             price_array.append()
#             for index, row in df.iterrows():
#                 save_market_data(date=date_list[-1], ticker=ticker, price=row['price'])
#                 break
#         else:
#             pass

        # for index, row in df.iterrows():
        #     daily_ticker(ticker=ticker, price=row['price'], date=date)
        #     print(row['price'], row['date'])
        #     save_market_data(date=row['date'], ticker=row['ticker'], price=row['price'])


def bulk_data_input(all_dates=True, date=datetime.now().strftime("%Y-%m-%d")):
    all_tickers = fasttrack.get_all_tickers(app_id=fasttrack.get_app_id())
    for ticker in all_tickers:
        df = fasttrack.compile_market_data(ticker=ticker, all_dates=all_dates, date=date)
        if df is not None:
            for index, row in df.iterrows():
                # daily_ticker(ticker=ticker, price=row['price'], date=date)
                print(row['price'], row['date'])
                save_market_data(date=row['date'], ticker=row['ticker'], price=row['price'])


bulk_data_input()
# bulk_data_input(all_dates=False)




    # if(datetime.)
