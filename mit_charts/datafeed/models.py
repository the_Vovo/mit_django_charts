from django.db import models


class DataFeed(models.Model):
    date = models.DateTimeField()
    ticker = models.CharField(max_length=15)
    price = models.FloatField()
