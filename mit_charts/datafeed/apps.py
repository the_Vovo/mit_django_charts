from django.apps import AppConfig


class DatafeedConfig(AppConfig):
    name = 'datafeed'

class UsersConfig(AppConfig):
    name = 'users'