from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, JsonResponse
from .models import DataFeed
from .fasttrack import get_all_tickers, get_app_id

#
# all_tickers = get_all_tickers(get_app_id())
#all_tickers = ['dick', 'hihihi', 'not_dick']
all_tickers = get_all_tickers(get_app_id())

@login_required()
def home(request):
    return render(request, 'datafeed/charts.html', {})


def get_data(request, ticker):
    data = {}
    prices = []
    dates = []
    all_dates = []
    global all_tickers
    datafeed = DataFeed.objects.filter(ticker=ticker)

    for object in datafeed:
        if object.date.strftime("%Y-%m-%d") not in all_dates:
            all_dates.append(object.date.strftime("%Y-%m-%d"))
    ticker_prices = []
    for object in datafeed:
        ticker_prices.append(object.price)
    data[ticker] = ticker_prices
    # data.append({'ticker': object.ticker,
    #              'price': object.price,
    #              'date': object.date})
    # if object.ticker == ticker:
    #     prices.append(object.price)
    #     dates.append(object.date.isoformat())
    # print(data.get('$SPXX'))
    return JsonResponse({'all_tickers': all_tickers, 'all_dates': all_dates, 'data': data}, safe=False)


def get_init_data(request):
    result = get_data(request=request, ticker=all_tickers)
    return result


def about(request):
    return render(request=request, template_name='datafeed/about.html')

