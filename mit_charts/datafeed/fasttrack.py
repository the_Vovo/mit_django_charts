import requests
import pandas as pd
from datetime import datetime

app_id = "5170c734-d947-412c-9d28-91a0d8772f4a"

def get_app_id():
    global app_id
    return app_id


def login(app_id):
    URL = "https://ftl.fasttrack.net/v1/auth/login"
    PARAMS = {"account": "301487",
            "pass": "0428XQGM",
        "pass": "0428XQGM",
        "appid": app_id}
    r = requests.get(url=URL, params=PARAMS)
    data = r.json()
    session_token = data["token"]
    return session_token


def get_all_tickers(app_id):
    session_token = login(app_id=app_id)
    URL = "https://ftlightning.fasttrack.net/v1/family/All%20Indexes"
    PARAMS = {"start": "",
              "unadj": "0",
              "olhv": "0"}
    HEADERS = {
        "appid": app_id,
        "token": session_token}
    BODY = "[\n  \"string\"\n]"
    r = requests.get(url=URL, headers=HEADERS)
    data = r.json()["tickers"].split(',')
    print(data)
    return data


count = 0


def get_bulk_market_dates(app_id):
    session_token = login(app_id=app_id)
    URL = "https://ftlightning.fasttrack.net/v1/data/dates"
    HEADERS= {
    "appid": app_id,
    "token": session_token}
    r = requests.get(url=URL, headers=HEADERS)
    print(r)
    dates = r.json()["dtes"]
    date_list = []
    for date in dates:
        date_object = datetime.strptime(date["strdate"], '%m/%d/%Y').date()
        # print(date["strdate"])
        date_list.append(date_object)
    return date_list


def get_bulk_market_data(app_id, ticker):
    session_token = login(app_id=app_id)
    URL = "https://ftlightning.fasttrack.net/v1/data/"
    HEADERS= {
    "appid": app_id,
    "token": session_token}
    # r = requests.get(url=URL + ticker, headers=HEADERS)
    r = requests.get(url=URL + ticker, params=HEADERS)
    data = r.json()
    #try:
    start_date = data["startdate"]
    market_data = list(data["prices"])
    for i in range(int(start_date["marketdate"])):
        market_data[i] = 0
    return market_data
    #except KeyError as e:
     #   pass


def compile_market_data(ticker, all_dates=True, date=''):
    global app_id
    # all_tickers = get_all_tickers(app_id=app_id)
    date_list = get_bulk_market_dates(app_id=app_id)
    df = pd.DataFrame(columns=['date', 'ticker', 'price'])
    price_array = get_bulk_market_data(app_id, ticker)
    temp_data = {'date': date_list, 'ticker': ticker, "price": price_array}
    # check if update all dates or just today + check if today is not holiday
    if not all_dates and date in date_list:
        date_index = date_list.index(date) # NEED TO FIX DATE FORMAT!!!
        temp_data = temp_data.get('price')[date_index]
        print(df)
        print("temp", temp_data)
    elif date not in date_list:
        return None
    print("temp", temp_data.get('date')[0])
    # print(date[0:4])
    temp_df = pd.DataFrame(data=temp_data, columns=['date', 'ticker', 'price'])
    result_df = df.append(temp_df, ignore_index=True)
    df = result_df
    print(df.iterrows())
    return df


compile_market_data(ticker="hi")
